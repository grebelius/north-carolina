#!/bin/bash

if [[ $# -eq 0 ]] ; then
	echo " "
	echo "    _______________________________________________"
	echo "   |                                               |"
	echo "   |  FIRST ARGUMENT: wget or curl                 |"
	echo "   | SECOND ARGUMENT: Path to list of urls         |"
	echo "   |  THIRD ARGUMENT: Path to download directory   |"
	echo "   |_______________________________________________|"
	echo " "
	exit 0
else
	STR=$3
	length=${#STR}
	last_char=${STR:length-1:1}
	[[ $last_char != "/" ]] && STR="$STR/"; :

	if [[ $1 == 'curl' ]] ; then
		cat $2 | parallel --bar -j30 curl -L {} -o $STR{/}
		exit 0
	else
		cat $2 | parallel --bar -j30 wget --quiet -nc -O $STR{/} {}
	fi
fi