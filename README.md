# INDIANA

## Att göra

- [x] Undersöka sajt
- [ ] Bestämma strategi (Court Commitment verkar innehålla all information vi vill ha. Det kommer handla om bash-script för att skapa csv-fil och sedan rensa med python-notebook. Dessutom finns bilder på http://webapps6.doc.state.nc.us/opi/viewpicture.do?method=view&showDate=N&pictureType=I&offenderID=_______)
- [ ] Mejla eller skapa ParseHub-projekt
- [ ] Hämta data
- [ ] Rensa med Jupyter Notebook
- [ ] Exportera csv-fil
- [ ] Exportera Excel-fil

http://webapps6.doc.state.nc.us/opi/viewpicture.do?method=view&showDate=N&pictureType=I&offenderID=1520420